<?php

define('VIDEO_STORAGE_DIR', '/media/raid0/storage/');
define('KARAOKE_STORAGE_DIR', '/media/raid0/karaoke/');
define('RECORDS_DIR', '/media/raid0/records/');
define('NFS_HOME_PATH', '/media/raid0/mac/');
// Use login and password from the configuration file. (api_auth_login and api_auth_password in server/custom.ini)
define('API_URL', 'http://api_login:api_password@stalker_ip/stalker_portal/api/');
define('PORTAL_URL', 'http://stalker_ip/stalker_portal/');
define('STORAGE_NAME', 'storage_name');
